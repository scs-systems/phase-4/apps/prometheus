cat <<EOT | kubectl apply -n web --force -f -
apiVersion: apps/v1
kind: Deployment
metadata:
  name: $CI_PROJECT_NAME
  namespace: web
  labels:
    app: $CI_PROJECT_NAME
spec:
  replicas: 1
  selector:
    matchLabels:
      app: $CI_PROJECT_NAME
  template:
    metadata:
      labels:
        app: $CI_PROJECT_NAME
    spec:
      containers:
      - name: $CI_PROJECT_NAME
        image: $PROD_REGISTRY_IMAGE:$SCSREL
        ports:
        - containerPort: 9090

---

apiVersion: v1
kind: Service
metadata:
  name: $CI_PROJECT_NAME
  namespace: web
spec:
  selector:
    app: $CI_PROJECT_NAME
  ports:
    - protocol: TCP
      port: 80
      targetPort: 9090
EOT

echo
kubectl -n web get service $CI_PROJECT_NAME
echo
kubectl -n web get deployment $CI_PROJECT_NAME
echo
kubectl -n web get pods -l app=$CI_PROJECT_NAME -o wide
